import fetch from 'node-fetch';
import UserService from './user.service';
import PreferenceService from './preference.service';
import RecommendationService from './recommendation.service';
import express from 'express';
import { checkPassword, hashPassword } from './hash';
import { genList } from './genlist';
import * as cp from 'child_process'
let spawn = cp.spawnSync

class UserController {
  constructor(
    public userService: UserService,
    public preferenceService: PreferenceService,
    public recommendationService: RecommendationService
  ) { }

  getUserInfo = async (req: express.Request, res: express.Response) => {
    try {
      if (req.session?.user?.username) {
        res.json({
          username: req.session?.user?.username,
          email: req.session?.user?.email,
          picture: req.session?.user?.picture
        });
        return
      }
      res.json({ success: false });
    } catch (error) {
      console.log("error", error)
      res.end("An error occur while get user info: ", error)
      return
    }
  }

  register = async (req: express.Request, res: express.Response) => {
    let { email, username, password, passwordRepeat, dishList, price } = req.body
    let emailError = ''
    let usernameError = ''
    let passwordError = ''
    let passwordRepeatError = ''
    let regUsername = /^[a-zA-Z0-9]{8,255}$/;
    let regEmail = /^\w+@[a-zA-Z0-9._-]+?\.[a-zA-Z]{2,3}$/;
    let regPassword = /(?=.*[a-z]).{6,}/ // /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,255}/;

    if (password != passwordRepeat) {
      passwordRepeatError = "Password don't match"
    }
    if (!regEmail.test(email)) {
      emailError = "Email is not valid";
    }

    if (!regUsername.test(username)) {
      usernameError = "Username doesn't match the required pattern";
    }

    if (!regPassword.test(password)) {
      passwordError = "Password doesn't match the required pattern"
    }

    try {
      let emailResult = await this.userService.loadUserByEmail({ email: email })
      if (emailResult[0] && !emailError) {
        emailError = "Email has been registered"
      }
      if (!emailError && !passwordError && !passwordRepeatError && !usernameError && dishList && price) {
        const userHashPW = await hashPassword(password)
        await this.userService.saveNewUser({
          username: username,
          password: userHashPW,
          email: email
        })
        const savedUser = await this.userService.loadUserByEmail({ email: email })
        await this.preferenceService.saveUserPreference({
          user_id: savedUser[0].id,
          preference: { dishList, price }
        })
        console.log('Loaded new user')
        let list = await genList(savedUser[0].id)
        console.log('Generating restaurants list')

        console.log(`list`, list)
        for (let eachItem in list) {
          // console.log('insert', list[eachItem])
          await this.recommendationService.saveEachUserRecommendation(savedUser[0].id, Number(list[eachItem]))
        }
        // console.log('Inserted restaurants list')

        res.redirect('/?verify=Your account is setup successfully. Please login to start.')
        return
      }
    } catch (error) {
      console.log("error", error)
      res.end("An error occur", error)
      return
    }
  }

  saveNewPreferenceAfterRegister = async (req: express.Request, res: express.Response) => {
    let { username, password, passwordRepeat, oldPassword, dishList, price } = req.body
    let usernameError = ''
    let passwordError = ''
    let passwordRepeatError = ''
    let regUsername = /^[a-zA-Z0-9]{8,255}$/;
    let regPassword = /(?=.*[a-z]).{6,}/ // /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,255}/;
    
    try {
      let user = await this.userService.loadUserByID({ id: req.session?.user.id })
      let checkUserOldPW = await checkPassword(oldPassword, user[0].password)

      if (!checkUserOldPW && !(oldPassword === '')) {
        res.json({ success: false, oldPasswordError: `Doesn't match previous password, please enter the correct password` });
        return
      }

      if (oldPassword && password && oldPassword == password) {
        res.json({ success: false, oldPasswordError: `New password is same with previous one, please enter another one` });
        return
      }

      if (password != passwordRepeat) {
        passwordRepeatError = "Password don't match"
      }

      if (!regUsername.test(username) && !(username === "")) {
        usernameError = "Username doesn't match the required pattern";
      }

      if (!regPassword.test(password) && !(password === "")) {
        passwordError = "Password doesn't match the required pattern"
      }

      if (!passwordError && !passwordRepeatError && !usernameError && dishList && price) {
        if (username) {
          await this.userService.updateUsername({ username: username, id: req.session?.user.id })
          if (req.session) { req.session.user.username = username }
        }

        if (password) {
          const userHashPw = await hashPassword(password)
          await this.userService.updateUserPassword({ password: userHashPw, id: req.session?.user.id })
          req.session?.destroy(function (err) { console.log(err) })
          res.json({ redirect: true })
        }

        let userPreference = await this.preferenceService.loadUserPreference(req.session?.user?.id)
        if (userPreference[0]) {
          await this.preferenceService.updateUserPreference({
            user_id: req.session?.user.id,
            preference: { dishList, price }
          })
        } else {
          await this.preferenceService.saveUserPreference({
            user_id: req.session?.user.id,
            preference: { dishList, price }
          })
        }

        // console.log('Loaded new user')
        let list = await genList(req.session?.user.id)
        // console.log('Generating restaurants list')

        // console.log(`list`, list)
        for (let eachItem in list) {
          // console.log('insert', list[eachItem])
          await this.recommendationService.saveEachUserRecommendation(req.session?.user.id, Number(list[eachItem]))
        }
        // console.log('Inserted restaurants list')
        res.json({ success: true, url: '/findrestaurant.html' })
        return
      }
      res.json({ success: false, passwordError: passwordError, usernameError: usernameError, passwordRepeatError: passwordRepeatError });


    } catch (error) {

    }
  }

  login = async (req: express.Request, res: express.Response) => {
    let { email, password, remember } = req.body
    // console.log('Got user info')

    try {
      let result = await this.userService.loadUserByEmail({ email: email })
      // console.log('Loaded user info: ', result)

      if (result[0]) {
        if (!result[0].is_active) {
          res.redirect('/?verify=Your account has not been verified')
          return
        }
        // console.log('Checking password')

        let checkPW = await checkPassword(password, result[0].password)
        // console.log('Password checked')

        if (checkPW) {
          // console.log('Ready to insert session')
          if (req.session) {
            // console.log('Inserting session')

            req.session.user = {
              id: result[0].id,
              username: result[0].username,
              email: result[0].email,
              picture: result[0].profile_picture
            };

            spawn('python', [
              "./AI/initmodel.py",
              String(req.session.user.id)
            ], { encoding: 'utf-8' })

            
            // console.log('Inserted session')
            if (remember) { req.session.cookie.maxAge = 2592000000; }
          }
          // console.log('Waiting to redirect')

          return res.redirect('/')
        } else {
          res.redirect("/?password=incorrect username or password");
        }
      }
    } catch (error) {
      // res.redirect("/?password=Error Occurred");
      console.log('Login controller error')
      return res.status(500).json(error.toString());
    }
  }

  loginGoogle = async (req: express.Request, res: express.Response) => {
    const accessToken = req.session?.grant.response.access_token;
    const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
      method: "get",
      headers: {
        "Authorization": `Bearer ${accessToken}`
      }
    });
    const result = await fetchRes.json()
    const user = await this.userService.loadUserByEmail({ email: result.email })
    // console.log('Google login: ', result)

    if (!user[0]) {
      const randomString = Math.random().toString(36).slice(-8)
      const userHashedPW = await hashPassword(randomString)
      const newUser = await this.userService.saveNewUserFromGoogle({
        username: result.name,
        password: userHashedPW,
        google_id: result.id,
        email: result.email,
        profile_picture: result.picture
      })
      if (req.session) {
        req.session.user = {
          id: newUser[0].id,
          username: newUser[0].username,
          email: newUser[0].email,
          picture: newUser[0].profile_picture
        }
        
      }
      return res.redirect('/')
    }

    if (user[0] && !user[0].google_id) {
      await this.userService.updateGoogleLoginUser({ google_id: result.id, email: result.email })
    }

    if (req.session) {
      req.session.user = {
        id: user[0].id,
        username: user[0].username,
        email: user[0].email,
        picture: user[0].profile_picture
      };
      spawn('python', [
        "./AI/initmodel.py",
        String(req.session.user.id)
      ], { encoding: 'utf-8' })
      console.log("load google")
    }
    return res.redirect('/')
  }

  logout = async (req: express.Request, res: express.Response) => {
    if (req.session) {
      req.session.destroy(function (err) {
        console.log(err)
      })
    }
    return res.redirect('/')
  }

}

export default UserController;