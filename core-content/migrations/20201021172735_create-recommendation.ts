import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('recommendation')) {
        return
    }
    await knex.schema.createTable('recommendation', table => {
        table.increments();
        table.integer('user_id').unsigned().notNullable().references('users.id');
        table.integer('shop_id').unsigned().notNullable().references('restaurants.id');
        table.boolean('is_recommended');
        table.timestamps(false);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('recommendation')
}

