import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('preferences')) {
    return
  }
  await knex.schema.createTable('preferences', table => {
    table.increments();
    table.integer('user_id').unsigned().notNullable().references('users.id');
    table.json('preference').notNullable();
    table.timestamps(false);
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('preferences')
}

