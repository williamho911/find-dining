import express from 'express';
import SearchRestaurantController from './searchrestaurant.controller';

export function searchRestaurantRouter(
  searchRestaurantController: SearchRestaurantController,
  isLoggedIn: express.RequestHandler
) {
  let searchRestaurantRoutes = express.Router();
  searchRestaurantRoutes.post('/recommendlist', isLoggedIn, searchRestaurantController.loadRecommendedRestaurants)

  return searchRestaurantRoutes;
}