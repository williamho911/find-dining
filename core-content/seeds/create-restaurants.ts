import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("restaurants").del();

    // Inserts seed entries
    let json = require('../../openrice_data/restaurants.json');
    json.forEach(element => {
        element.address = element.location
        delete element.location
        element.smile = element.smiles
        delete element.smiles
    });
    await knex("restaurants").insert(json);
};
