import DiningRecordService from './diningrecord.service';
import RecommendationService from './recommendation.service';
import express from 'express';

class DiningRecordController {
  constructor(
    public diningRecordService: DiningRecordService,
    public recommendationService: RecommendationService
  ) {}

  post = async (req: express.Request, res: express.Response) => {
    console.log(1)
    try {
      if (req.session?.user?.id) {
    console.log(2)

        const newRecord = await this.diningRecordService.saveDiningRecord(req.session?.user?.id, req.body.shop_id, req.body.choice)
        console.log('New dining record: ', newRecord)
        await this.recommendationService.updateRecommendRestaurantStatus(req.session?.user.id, req.body.shop_id)
        res.json({ success: true });
      }
    } catch (error) {
      res.json({ success: false })
    }
  }
}

export default DiningRecordController