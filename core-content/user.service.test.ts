import UserService from './user.service';
import Knex from 'knex';
import { User, UserOfGoogle } from './models';
import { checkPassword, hashPassword } from './hash';
const knexConfig = require('./knexfile')

describe('Login Service Testsuit', () => {
  let knex: Knex;
  let userService: UserService;
  let initUser : User = {
    id: 1,
    username: 'william',
    email: 'william@gmail.com',
    password: '123456',
    is_active: true
  }

  beforeAll(async () => {
    knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);
    await knex('users').del();
    await knex('users')
      .insert(initUser)

    userService = new UserService(knex)
  });

  afterAll(() => {
    knex.destroy();
  });

  test('load user email', async () => {
    let userOne = await userService.loadUserByEmail({ email: initUser.email })
    expect(userOne).toBeDefined()
    expect(userOne[0].username).toBe('william')
  });

  test('load user ID', async() => {
    let user = await userService.loadUserByID({ id : initUser.id })
    expect(user).toBeDefined()
    expect(user[0].username).toBe('william')
    expect(user[0].id).toBe(1)
  })

  test('save new user', async() => {
    let initUser2 : User = {
      username: 'jack',
      email: 'jack@gmail.com',
      password: '123456',
      is_active: true
    }
    await userService.saveNewUser(initUser2)
    let users = await knex.select('id').from('users')
    expect(users.length).toBe(2)
  })

  test('update user name', async () => {
    let testUser:User = {username: 'stupidwilliam', id: 1}
    await userService.updateUsername(testUser)
    let name = await knex.select('username').from('users').where('id', testUser.id)
    expect(name[0].username).toBe(testUser.username)
  })

  test('update user password',async () => {
    let hashed = await hashPassword('654321')
    let testUser:User = {password: hashed, id: 1}
    await userService.updateUserPassword(testUser)
    let pw = await knex.select('password').from('users').where('id', testUser.id)
    expect(await checkPassword('654321',pw[0].password)).toBe(true)
  })

  test('save user from google', async () => {
    let googleUser: UserOfGoogle = {
      username: 'googlejack',
        password: '123456',
        google_id: 'googleid',
        email: 'google@gmail.com',
        profile_picture: 'picture.jpg'
    }
    await userService.saveNewUserFromGoogle(googleUser)
    let users = await knex.select('id').from('users')
    expect(users.length).toBe(3)
  })

  test('update google login user', async () => {
    let initUser3 : User = {
      username: 'jack',
      email: 'jack2@gmail.com',
      password: '123456',
      is_active: true
    }
    let googleUser: UserOfGoogle = {
        google_id: 'googleid',
        email: 'jack2@gmail.com',
    }
    await userService.saveNewUser(initUser3)
    await userService.updateGoogleLoginUser(googleUser)
    let user = await knex.select('google_id').from('users').where('email', googleUser.email)
    expect(user[0].google_id).toBe(googleUser.google_id)
  })

  // test('updateEmail', async () => {
  //   let updatedEmail = await userService.updateEmail({ email: myEmail, update_email: 'williamho911@outlook.com' })
  //   console.log(updatedEmail[0])
  //   expect(updatedEmail).toHaveLength(1)
  //   expect(updatedEmail[0]).toBe('williamho911@outlook.com')
  // })
})