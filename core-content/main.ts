import express from 'express';
import expressSession from 'express-session';
import bodyParser from "body-parser";
import grant from 'grant-express';
import Knex from 'knex';
import knexConfigs from './knexfile';
import dotenv from 'dotenv';
import fs from 'fs';
import * as cp from "child_process";
import * as scheduler from 'node-schedule';
let spawn = cp.spawn

let mode = process.env.NODE_ENV || 'development';
let knexConfig = knexConfigs[mode]
export const knex = Knex(knexConfig)
export const env = dotenv.parse(fs.readFileSync('.env.' + mode).toString())
// console.log(env.GOOGLE_CLIENT_ID)

// basic settings
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressSession({
  secret: 'Find your best dinning place',
  resave: true,
  saveUninitialized: true
}));

//Google OAuth
app.use(
  grant({
    "defaults": {
      "protocol": "https",
      "host": "finddining.ml",
      "transport": "session",
      "state": true,
    },
    "google": {
      "key": env.GOOGLE_CLIENT_ID || "",
      "secret": env.GOOGLE_CLIENT_SECRET || "",
      "scope": ["profile", "email"],
      "callback": "/login/google"
    },
  }));

// app.get('/personalinfo', async (req, res) => {
//   if (req.session?.user?.username) {
//     res.json({ 
//       username: req.session?.user?.username, 
//       email: req.session?.user?.email, 
//       picture: req.session?.user?.picture });
//     return
//   }
//   res.json({ success: false });
// })

import { userRouter } from './user.routes'
import UserController from './user.controller';
import UserService from './user.service';
import { searchRestaurantRouter } from './searchrestaurant.routes';
import SearchRestaurantController from './searchrestaurant.controller';
import SearchRestaurantService from './searchrestaurant.service';
import { diningRecordRouter } from './diningrecord.routes';
import DiningRecordController from './diningrecord.controller';
import DiningRecordService from './diningrecord.service';
import PreferenceService from './preference.service';
import RecommendationService from './recommendation.service';
import { isLoggedIn, isInit } from './guards'

let userService = new UserService(knex)
let searchRestaurantService = new SearchRestaurantService(knex)
let diningRecordService = new DiningRecordService(knex)
let preferenceService = new PreferenceService(knex)
let recommendationService = new RecommendationService(knex)


let userController = new UserController(userService, preferenceService, recommendationService)
let userRoutes = userRouter(userController)
app.use('/', userRoutes);

let searchRestaurantController = new SearchRestaurantController(searchRestaurantService, diningRecordService)
let searchRestaurantRoutes = searchRestaurantRouter(searchRestaurantController, isLoggedIn)
app.use('/', searchRestaurantRoutes)

let diningRecordController = new DiningRecordController(diningRecordService, recommendationService)
let diningRecordRoutes = diningRecordRouter(diningRecordController)
app.use('/', diningRecordRoutes)

// app.post('/postdata', async (req, res) => {
//   console.log(req.body)
//   // if(req.session?.user?.id){
//   if (req.session?.user?.id) {
//     let id = req.session?.user?.id
//     const ids = await knex.insert({
//       user_id: id,
//       restaurant_id: req.body.shop_id,
//       choice: req.body.choice,
//       created_at: knex.fn.now(),
//       updated_at: knex.fn.now()
//     }).into("dinning_records")
//       .returning('id');
//     console.log("ids", ids)
//     const recom = await knex('recommendation').update({
//       is_recommended: true,
//     }).where({ 'user_id': id, 'shop_id': req.body.shop_id }).returning("user_id");
//     console.log("recom", recom)
//     res.json({ success: true });
//     return
//     console.log(id)
//   } else {
//     res.json({ success: false })
//     return
//   }
// })

// app.use('/findrestaurant.html', isLoggedIn, isInit, (req, res, next) => {
//   next()
// })

app.use('/recommendrestaurant', async (req, res) => {
  if (req.session?.restaurants) {
    res.json({
      success: true,
      list: req.session?.restaurants
    })
  } else {
    res.json({ success: false })
  }
})
//-------------------------Testing AI python return restaurants id------------------------------------
app.get('/call/python', aimodel)

function aimodel(req, res) {
  let process1 = spawn('python', [
    "./AI/testmodel.py"
  ])

  process1.stdout.on('data', (data) => {
    const parsedString = JSON.parse(data)
    res.json(parsedString)
})
}
//-------------------------Testing AI python return restaurants id------------------------------------

// router below these two app.use will be redirected to 404 !!!!
app.use(express.static('AI'));
app.use(express.static('public'));
// isInit may not be useful;
app.use(isLoggedIn, isInit, express.static('private'));

// set up the server
const PORT = 8080;

app.listen(PORT, () => {
  console.log(`Server runs at ${PORT} in ${mode} mode.`);
  scheduler.scheduleJob('0 0 0 * * *', function(){
    // write the daily update recommendation app here 
    console.log(`Please write the function here`)
    })
});

app.use((req, res) => {
  res.redirect('/404.html')
  console.log('URL: ', req.url, '; METHOD: ', req.method)
});

