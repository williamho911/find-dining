import * as cp from "child_process"

export async function genList(id) {
  let spawn = await cp.spawnSync
  
  let process1 = await spawn('python', [
    "./AI/genliststat.py", id
  ],
    { encoding: 'utf-8' })

  let list = JSON.parse(process1.stdout)

  let recommlist = JSON.parse(list['recommlist'])

  return recommlist

}