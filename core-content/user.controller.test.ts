import UserController from './user.controller';
import UserService from './user.service';
import PreferenceService from './preference.service';
import RecommendationService from './recommendation.service';
import express from 'express';
import * as Knex from 'knex';

// let User = require.main?.require('./models/User')

describe('Login Controller Testsuit', () => {
  let userController: UserController;
  let userService: UserService;
  let preferenceService: PreferenceService;
  let recommendationService: RecommendationService
  let req: express.Request;
  let res: express.Response;

  beforeEach(() => {
    userService = new UserService({} as Knex);
    preferenceService = new PreferenceService({} as Knex)
    recommendationService = new RecommendationService({} as Knex)

    jest
      .spyOn(userService, "saveNewUser")
      .mockImplementation(async () => []) //{ id: 1, username: 'rylox', email: 'rylox@gmail.com', password: '123456', is_active: true, passwordRepeat: '123456', dishList: ["Japanese", "Thai"], price: '101-200' }

    jest
      .spyOn(userService, 'loadUserByEmail')
      .mockImplementation(async () => [{ email: 'william@gmail.com' }])

    jest
      .spyOn(preferenceService, 'saveUserPreference')
      .mockImplementation(async () => [{ user_id: 1, preference: { dishList: ["Japanese", "Thai"], price: "101-200" } }]) //

    jest
      .spyOn(recommendationService, 'saveEachUserRecommendation')
      .mockImplementation(async () => [])

    userController = new UserController(userService, preferenceService, recommendationService)
    req = {} as express.Request;
    res = {
      json: jest.fn(),
      redirect: jest.fn(),
      end: jest.fn(),
      status: jest.fn(() => {
        return { json: (data: any) => { console.log(data) } }
      })
    } as any;
  })

  it.skip('should load a user with his / her email ', async () => {
    // checkPassword = jest.fn
    req.body = {
      email: 'william@gmail.com',
      password: '123456',
      remember: true
    } as any

    await userController.login(req, res);
    expect(userService.loadUserByEmail).toBeCalled()
    expect(userService.loadUserByEmail).toHaveBeenLastCalledWith({ email: 'william@gmail.com' })
    expect(res.redirect).toBeCalledWith('/?verify=Your account has not been verified')
  })

  it('should save a new user', async () => {
    req.body = {
      username: 'rylox',
      email: 'rylox@gmail.com',
      password: '123456',
      passwordRepeat: '123456',
      dishList: ["Japanese", "Thai"],
    } as any

    await userController.register(req, res)
    await userService.saveNewUser({
      username: 'rylox',
      email: 'rylox@gmail.com',
      password: '123456',
    })
    expect(req.body.password).toBe(req.body.passwordRepeat)
    expect(userService.loadUserByEmail).toBeCalledWith({"email": "rylox@gmail.com"})
    expect(userService.saveNewUser).toBeCalledWith({"email": "rylox@gmail.com", "password": "123456", "username": "rylox"})
    // expect(res.redirect).toBe('/?verify=Your account is setup successfully. Please login to start.')
    // expect(preferenceService.saveUserPreference).toBeCalledWith({ preference: { dishList: ["Japanese", "Thai"], price: "101-200" } })
    // expect(recommendationService.saveEachUserRecommendation).toBeCalled()
  })
})
