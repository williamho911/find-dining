//Limit checkbox not more than 3
let buttons = document.querySelectorAll(".buttons");
let maxChecked = 3;
for (let i = 0; i < buttons.length; i++) {
  buttons[i].onclick = selectiveCheck;
}
function selectiveCheck() {
  let checkedChecks = document.querySelectorAll(".buttons:checked");
  if (checkedChecks.length >= maxChecked + 1) {
    return false;
  }
}

let submit = document.querySelector("#findRestaurantForm");
submit.addEventListener("submit", async (event) => {
  event.preventDefault();
  let activeAreas = document.querySelectorAll(".buttons:checked");
  let areaList = [];
  for (let area of activeAreas) {
    areaList.push(area.value);
  }
  console.log(JSON.stringify({location: areaList}));
  if (activeAreas.length) {
    let res = await fetch("/recommendlist", {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify({
        location: areaList,
      }),
    });
    let result = await res.json()

    if (Object.keys(result.list).length > 0) {
      location.href = result.url;
      return 
    }

    document.querySelector(".message").innerHTML = `
      <p>There are no recommended restaurants according to your options,
      please try other areas.</p>`;
  } else {
    document.querySelector(".message").innerHTML = `<p>Please choose the area before search.</p>`;
  }
});

async function loadUser() {
  const res = await fetch('/personalinfo', {
      method: "get",
  });
  const result = await res.json();
  if (result.username) {
      if(result.picture){
          document.querySelector("#usernameDiv").innerHTML = `<img src="${result.picture}" alt="userpicture"  height="30px" width="30px"> ${result.username}`
      }else{
          document.querySelector("#usernameDiv").innerHTML = `<div alt="userpicture" height="30px" width="30px"> ${result.username}</div>`
      }
  }

  console.log(result)
}

loadUser();