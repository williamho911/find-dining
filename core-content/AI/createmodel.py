#%%
import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import string
import json
import numpy
import psycopg2
import os
from dotenv import load_dotenv

load_dotenv()
print(os.getenv('DB_USER'))

con = psycopg2.connect(
  host = os.getenv('DB_HOST'),
  database = os.getenv('DB_NAME'),
  user = os.getenv('DB_USER'),
  password = os.getenv('DB_PASSWORD')
)

#Create function to convert database data to 7 params list and data cleaning
def convert(userPreference):
  init = []
  i = 0
  for row in userPreference: # each row is a list
    init.append(row)
    if len(row[1].split('|')) == 2:
      types = row[1].split('|')
      init[i].insert(1, types[0])
      init[i].insert(2, types[1].replace('/', '').replace(' ', ''))
      init[i].pop(3)
    else:
      types = row[1].split('|')
      init[i].insert(1, types[0])
      init[i].insert(2, '0')
      init[i].pop(3)
    i += 1
  space = " "
  results = []
  labels = []
  print(len(init))
  for each in init:
    results.append(space.join(str(x) for x in each)[:-1])
    labels.append(each[-1])
  return results, labels

#connect database and retreive data from dining_records
cur = con.cursor()
cur.execute("SELECT id from users")
usersid = cur.fetchall()
for userid in usersid:
  id = userid[0]
  print(id)
  cur.execute(f"SELECT price, category, reviews, smile, cry, rating, choice FROM restaurants INNER JOIN dinning_records ON restaurants.id = restaurant_id where user_id = {id}")
  userData = cur.fetchall()
  #newlist = dinning_records of one user
  if len(userData) > 9:
    newlist = []
    for row in userData:
      newlist.append(list(row))
    #Converted data
    convertedResults = convert(newlist)
    convertedFeatures = convertedResults[0]
    convertedLabels = convertedResults[1]
    #Tokenization of data
    tokenizer = Tokenizer(num_words = 500)
    tokenizer.fit_on_texts(convertedFeatures)
    training = tokenizer.texts_to_sequences(convertedFeatures)
    padded = pad_sequences(training)
    array = np.array(padded)
    print(array.shape)
    #convert list to tensorflow format
    train_data = tf.constant(array, dtype=tf.int32)
    train_labels = tf.constant(np.array(convertedLabels), dtype=tf.int32)
    print("training: {}, labels: {}".format(len(train_data), len(train_labels)))
    #model config
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Embedding(10000, 16))
    model.add(tf.keras.layers.GlobalAveragePooling1D())
    model.add(tf.keras.layers.Dense(16, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(1, activation=tf.nn.sigmoid))
    model.summary()
    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    #run AI model
    history = model.fit(train_data,
                        train_labels,
                        epochs=100,
                        validation_data=None,
                        verbose=1)
    #save model for each users
    model.save(f'./user{id}.h5', overwrite=True)
    test_data = train_data
    print(test_data)
    print(model.predict(test_data))
  
con.close()
# %%
# from tensorflow import keras
# new_model = keras.models.load_model("test.h5")
# new_model.predict(test_data)