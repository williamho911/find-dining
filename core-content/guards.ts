import { Request, Response, NextFunction } from 'express';

import Knex from 'knex';
import knexConfigs from './knexfile';
import PreferenceService from './preference.service';

let mode = process.env.NODE_ENV || 'development';
let knexConfig = knexConfigs[mode]
const knex = Knex(knexConfig)

let preferenceService = new PreferenceService(knex)

export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
    if (req.session && req.session.user != null) {
        next();
    } else {
        res.redirect('/404.html');
    }
}

export async function isInit(req: Request, res: Response, next: NextFunction) {
    // if (!req.session?.user) {
    //     res.redirect('/')
    // }

    if (req.session?.user) {
        // Select by is_alive better
        const preference = await preferenceService.loadUserPreference(req.session.user?.id)
        // Should do a better logging  using logger like winston
        // console.log(pet)

        if (preference[0]) {
            // if (req.originalUrl) {
            //     console.log(req.originalUrl)
            //     res.redirect(req.originalUrl);

            // }
            next();
        } else {
            const regex = new RegExp(/.html/i);
            let checkHTML = regex.test(req.originalUrl)
            if (req.originalUrl !== '/useredit.html' && checkHTML) {
                res.redirect('/useredit.html');
                return;
            }
            next();
            return
        }
    }
}

// // How about this?
// export async function isInit(req: Request, res: Response, next: NextFunction) {
//     if (!req.session?.user) {
//         res.redirect('/')
//     }
//     if (req.session?.user) {
//         // Select by is_alive better
//         const preference = await preferenceService.loadUserPreference(req.session.user.id)
//         // Should do a better logging  using logger like winston
//         // console.log(pet)

//         if(preference[0]) {
//             if(req.originalUrl === "useredit.html") {
//                 res.redirect("findrestaurant.html");
//                 return;
//             }
//             next();
//             return;
//         } else {
//             const regex = new RegExp(/.html/i);
//             let checkHTML = regex.test(req.originalUrl)
//             if (req.originalUrl !== '/useredit.html' && checkHTML) {
//                 res.redirect('/useredit.html');
//                 return;
//             }
//             next();
//             return
//         }
//     }
// }