import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('restaurants')
    await knex.schema.dropTableIfExists('preferences')
    await knex.schema.dropTableIfExists('dinning_categories')
    await knex.schema.dropTableIfExists('restaurant_districts')
    await knex.schema.dropTableIfExists('price_ranges')
    await knex.schema.dropTableIfExists('dining_records')
    if (await knex.schema.hasTable('restaurants')) {
        return
    }
    await knex.schema.createTable('restaurants', table => {
        table.increments();
        table.string('name', 255).notNullable();
        table.string('district', 255).notNullable();
        table.string('address', 255).notNullable();
        table.string('price', 255).notNullable();
        table.string('category', 255).notNullable();
        table.integer('reviews');
        table.integer('smile');
        table.integer('cry');
        table.float('rating');
        table.string('link', 255).notNullable();
        table.string('photo', 255)
    })
    if (await knex.schema.hasTable('dinning_records')) {
        return
    }
    await knex.schema.createTable('dinning_records', table => {
        table.increments();
        table.integer('user_id').unsigned().notNullable().references('users.id');
        table.integer('restaurant_id').unsigned().notNullable().references('restaurants.id');
        table.timestamps(false);
        table.float('choice').notNullable();
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.createTable('preferences', table => {
        table.increments();
        table.timestamps(false, true);
        table.integer('user_id').unsigned().notNullable().references('users.id');
        table.integer('dinning_category_id').unsigned().notNullable().references('dinning_categories.id');
        table.integer('price_range_id').unsigned().notNullable().references('price_ranges.id');
        table.jsonb('user_preferences');
    })
    await knex.schema.createTable('dinning_categories', table => {
        table.increments();
        table.string('category', 255).notNullable();
    })
    await knex.schema.createTable('price_ranges', table => {
        table.increments();
        table.string('range', 255).notNullable();
    })
    await knex.schema.createTable('restaurant_districts', table => {
        table.increments();
        table.string('district', 255).notNullable();
    })
    await knex.schema.dropTableIfExists('restaurants')
    await knex.schema.dropTableIfExists('dining_records')
    await knex.schema.createTable('restaurants', table => {
        table.increments();
        table.string('name', 255).notNullable();
        table.string('address', 255).notNullable();
        table.string('link');
        table.integer('smile');
        table.integer('cry');
        table.integer('reviews');
        table.float('rating');
        table.integer('dinning_category_1_id').unsigned().notNullable().references('dinning_categories.id');
        table.integer('dinning_category_2_id').unsigned().notNullable().references('dinning_categories.id');
        table.integer('price_range_id').unsigned().notNullable().references('price_ranges.id');
        table.integer('restaurant_district_id').unsigned().notNullable().references('restaurant_districts.id');
    })
}

