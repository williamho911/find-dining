import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('preferences')) {
    return
  }
  await knex.schema.createTable('preferences', table => {
    table.increments();
    table.timestamps(false, true);
    table.integer('user_id').unsigned().notNullable().references('users.id');
    table.integer('dinning_category_id').unsigned().notNullable().references('dinning_categories.id');
    table.integer('price_range_id').unsigned().notNullable().references('price_ranges.id');
    table.jsonb('user_preferences');
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('preferences')
}

